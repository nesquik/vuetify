"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.attachedRoot = attachedRoot;

/**
 * Returns:
 *  - 'null' if the node is not attached to the DOM
 *  - the root node (HTMLDocument | ShadowRoot) otherwise
 */
function attachedRoot(node) {
  /* istanbul ignore next */
  var frameDoc = document.querySelector('[data-iframe-plugin=""]');

  if (typeof node.getRootNode !== 'function') {
    // Shadow DOM not supported (IE11), lets find the root of this node
    while (node.parentNode) {
      node = node.parentNode;
    } // The root parent is the document if the node is attached to the DOM


    if (node !== document) return null; // eslint-disable-next-line

    console.log(frameDoc);
    return document;
  }

  var root = node.getRootNode(); // The composed root node is the document if the node is attached to the DOM

  if (root !== document && root.getRootNode({
    composed: true
  }) !== document) return null; // eslint-disable-next-line

  console.log(frameDoc);
  return root;
}
//# sourceMappingURL=dom.js.map